﻿
namespace DHT22Sensor
{
    partial class Form1
    {
        /// <summary>
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer üretilen kod

        /// <summary>
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.comboBox_baudRate = new System.Windows.Forms.ComboBox();
            this.comboBox_portLists = new System.Windows.Forms.ComboBox();
            this.buton_ac = new System.Windows.Forms.Button();
            this.buton_kapat = new System.Windows.Forms.Button();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.sinyal = new System.Windows.Forms.Label();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label_humidity = new System.Windows.Forms.Label();
            this.label_temperature = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBox_baudRate
            // 
            this.comboBox_baudRate.FormattingEnabled = true;
            this.comboBox_baudRate.Items.AddRange(new object[] {
            "9600",
            "38400",
            "57600",
            "115200"});
            this.comboBox_baudRate.Location = new System.Drawing.Point(178, 555);
            this.comboBox_baudRate.Name = "comboBox_baudRate";
            this.comboBox_baudRate.Size = new System.Drawing.Size(119, 36);
            this.comboBox_baudRate.TabIndex = 3;
            this.comboBox_baudRate.Text = "57600";
            // 
            // comboBox_portLists
            // 
            this.comboBox_portLists.FormattingEnabled = true;
            this.comboBox_portLists.Location = new System.Drawing.Point(53, 555);
            this.comboBox_portLists.Name = "comboBox_portLists";
            this.comboBox_portLists.Size = new System.Drawing.Size(119, 36);
            this.comboBox_portLists.TabIndex = 2;
            this.comboBox_portLists.DropDown += new System.EventHandler(this.comboBox_portLists_DropDown);
            // 
            // buton_ac
            // 
            this.buton_ac.Location = new System.Drawing.Point(53, 612);
            this.buton_ac.Name = "buton_ac";
            this.buton_ac.Size = new System.Drawing.Size(119, 38);
            this.buton_ac.TabIndex = 1;
            this.buton_ac.Text = "AÇ";
            this.buton_ac.UseVisualStyleBackColor = true;
            this.buton_ac.Click += new System.EventHandler(this.buton_ac_Click);
            // 
            // buton_kapat
            // 
            this.buton_kapat.Location = new System.Drawing.Point(178, 612);
            this.buton_kapat.Name = "buton_kapat";
            this.buton_kapat.Size = new System.Drawing.Size(119, 38);
            this.buton_kapat.TabIndex = 1;
            this.buton_kapat.Text = "KAPAT";
            this.buton_kapat.UseVisualStyleBackColor = true;
            this.buton_kapat.Click += new System.EventHandler(this.buton_kapat_Click);
            // 
            // chart1
            // 
            this.chart1.BackColor = System.Drawing.Color.Transparent;
            chartArea1.BackColor = System.Drawing.Color.Transparent;
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            this.chart1.Location = new System.Drawing.Point(680, 57);
            this.chart1.Name = "chart1";
            series1.BorderColor = System.Drawing.Color.Black;
            series1.BorderWidth = 3;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series1.Color = System.Drawing.Color.DarkRed;
            series1.MarkerStep = 5;
            series1.Name = "Humidity";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(688, 483);
            this.chart1.TabIndex = 3;
            this.chart1.Text = "chart1";
            // 
            // serialPort1
            // 
            this.serialPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort1_DataReceived);
            // 
            // sinyal
            // 
            this.sinyal.AutoSize = true;
            this.sinyal.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.sinyal.Location = new System.Drawing.Point(320, 609);
            this.sinyal.Name = "sinyal";
            this.sinyal.Size = new System.Drawing.Size(0, 32);
            this.sinyal.TabIndex = 6;
            // 
            // chart2
            // 
            this.chart2.BackColor = System.Drawing.Color.Transparent;
            this.chart2.BorderlineColor = System.Drawing.Color.Transparent;
            chartArea2.BackColor = System.Drawing.Color.Transparent;
            chartArea2.Name = "ChartArea1";
            this.chart2.ChartAreas.Add(chartArea2);
            this.chart2.Location = new System.Drawing.Point(0, 57);
            this.chart2.Name = "chart2";
            this.chart2.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Grayscale;
            series2.BorderColor = System.Drawing.Color.Black;
            series2.BorderWidth = 3;
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series2.Color = System.Drawing.Color.RoyalBlue;
            series2.Name = "Temperature";
            this.chart2.Series.Add(series2);
            this.chart2.Size = new System.Drawing.Size(703, 483);
            this.chart2.TabIndex = 3;
            this.chart2.Text = "chart1";
            // 
            // label_humidity
            // 
            this.label_humidity.AutoSize = true;
            this.label_humidity.Font = new System.Drawing.Font("Arial", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label_humidity.Location = new System.Drawing.Point(718, 9);
            this.label_humidity.Name = "label_humidity";
            this.label_humidity.Size = new System.Drawing.Size(414, 70);
            this.label_humidity.TabIndex = 8;
            this.label_humidity.Text = "Sıcaklık = ... °";
            // 
            // label_temperature
            // 
            this.label_temperature.AutoSize = true;
            this.label_temperature.Font = new System.Drawing.Font("Arial", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label_temperature.Location = new System.Drawing.Point(26, 8);
            this.label_temperature.Name = "label_temperature";
            this.label_temperature.Size = new System.Drawing.Size(469, 70);
            this.label_temperature.TabIndex = 7;
            this.label_temperature.Text = "Basınç = ... kPa";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 28F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1417, 685);
            this.Controls.Add(this.label_humidity);
            this.Controls.Add(this.label_temperature);
            this.Controls.Add(this.comboBox_baudRate);
            this.Controls.Add(this.sinyal);
            this.Controls.Add(this.comboBox_portLists);
            this.Controls.Add(this.chart2);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.buton_kapat);
            this.Controls.Add(this.buton_ac);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TurboJet Sensör İzleme";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox comboBox_baudRate;
        private System.Windows.Forms.ComboBox comboBox_portLists;
        private System.Windows.Forms.Button buton_ac;
        private System.Windows.Forms.Button buton_kapat;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.Label sinyal;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
        private System.Windows.Forms.Label label_humidity;
        private System.Windows.Forms.Label label_temperature;
    }
}

